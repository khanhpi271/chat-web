import { FC } from 'react';
import styled, { css } from 'styled-components';

type InputProps = {
  size?: 'small' | 'medium' | 'large';
  variant?: 'primary' | 'secondary';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onChange: (event: any) => void;
  disabled?: boolean;
};

const StyledInput = styled.input<Pick<InputProps, 'size' | 'variant'>>`
  border: none;
  border-radius: 4px;
  height: ${({ size }) => (size === 'large' ? '40px' : size === 'small' ? '32px' : '38px')};
  outline: none;
  transition:
    background-color 0.3s,
    box-shadow 0.3s;

  ${({ variant }) =>
    variant === 'primary' &&
    css`
      background-image: ${({ theme }) => theme.colors.primaryGradient};
      color: ${({ theme }) => theme.colors.light};

      &:hover {
        background-color: #0056b3;
      }
    `}

  ${({ disabled }) =>
    disabled &&
    css`
      background-color: ${({ theme }) => theme.colors.gray400};
      color: ${({ theme }) => theme.colors.dark700};
      cursor: not-allowed;

      &:hover {
        background-color: ${({ theme }) => theme.colors.gray400}; // same as the disabled background
      }
    `}
`;

const Input: FC<InputProps> = ({ size = 'medium', variant = 'primary', onChange, disabled = false }) => {
  return (
    <StyledInput variant={variant} onChange={onChange} disabled={disabled}>
      {/* {label} */}
      {/* <input /> */}
    </StyledInput>
  );
};

export default Input;
