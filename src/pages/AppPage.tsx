// In AppPage.tsx

import { FC, lazy, ReactElement, Suspense } from 'react';
// import Row from 'src/layout/components/Row';
import CircularPageLoader from 'src/shared/page-loader/CircularPageLoader';

const Chat = lazy(() => import('src/features/chat'));
const Header = lazy(() => import('src/shared/header/components/Header'));

const AppPage: FC = (): ReactElement => {
  console.log('234');

  return (
    <div>
      <Suspense fallback={<CircularPageLoader />}>
        <Header />
        <Chat />
        {/* <Row marginTop={'2rem'}>
          <Footer />
        </Row>{' '} */}
      </Suspense>
    </div>
  );
};

export default AppPage;
