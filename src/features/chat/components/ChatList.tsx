import React from 'react';
import Row from 'src/layout/components/Row';
import { device } from 'src/theme';
import styled from 'styled-components';

import ChatItem from './ChatItem';

const data = [
  {
    id: 1,
    name: 'ABasdasdC',
    avatar: 'https://www.simplilearn.com/ice9/free_resources_article_thumb/what_is_image_Processing.jpg',
    message: 'Nwa '
  },
  {
    id: 1,
    name: 'ABC',
    avatar: '123asd',
    message: 'Nwa '
  },
  {
    id: 1,
    name: 'ABC',
    avatar: '123asd',
    message: 'Nwa '
  },
  {
    id: 1,
    name: 'ABC',
    avatar: '123asd',
    message: 'Nwa '
  }
];

const ChatWrapper = styled.div`
  width: 100%;

  padding: 15px 10%;

  display: flex;
  flex-direction: column;
  gap: 3rem;

  @media ${device.small} {
    padding: 15px 5%;
  }

  @media ${device.medium} {
    padding: 15px 10%;
  }

  @media ${device.large} {
    padding: 15px 10%;
  }
`;

const ChatList = () => {
  return (
    <ChatWrapper>
      {data?.map((chat, i) => (
        <React.Fragment key={i}>
          <ChatItem key={chat.id} name={chat.name} image={chat.avatar} message={chat.message} />
        </React.Fragment>
      ))}
    </ChatWrapper>
  );
};

export default ChatList;
