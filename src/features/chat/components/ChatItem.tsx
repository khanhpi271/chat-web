import { FC, ReactElement } from 'react';
import Row from 'src/layout/components/Row';
import styled from 'styled-components';

interface ChatItemProps {
  id: string;
  name: string;
  image: string;
  message: string;
  date?: string; //yyyy-mm-dd
  isActive?: boolean;
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.light};
`;

const StyleChatItem = styled.div<{ image: string }>`
  background-image: url(${({ image }) => image});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  position: relative;
  width: 50px;
  height: 50px;
  border-radius: 4px;
`;

const StyleInfoItem = styled.div`
  color: ${({ theme }) => theme.colors.gray500};
  display: flex;
  flex-direction: column;
  width: 100%;
  padding-left: 5px;
`;

const TitlePost = styled.h2`
  color: ${({ theme }) => theme.colors.dark700};
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  cursor: pointer;

  &:hover {
    color: ${({ theme }) => theme.colors.primary600};
    text-decoration: underline;
  }
`;

const TagPost = styled.h3`
  color: ${({ theme }) => theme.colors.primary600};

  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`;

const ChatItem: FC<ChatItemProps> = (props): ReactElement => {
  const { image, name, message } = props;
  console.log('name', name, message);

  return (
    <Wrapper>
      <StyleChatItem image={image}>
        {/* <StyleInfoItem>
          <DateTimeDisplay dateString={date} inline />
        </StyleInfoItem> */}
      </StyleChatItem>
      {/* <TitlePost>{title}</TitlePost> */}
      <Row gap="0.5rem">
        <StyleInfoItem>
          <span className="mr-2">{name}</span>
          <span className="mr-2">{message}</span>
        </StyleInfoItem>
      </Row>
    </Wrapper>
  );
};

export default ChatItem;
