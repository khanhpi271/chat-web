import { lazy } from 'react';
import Row from 'src/layout/components/Row';
import InputSearch from 'src/shared/input/InputCustom';
import styled from 'styled-components';

const Sidebar = styled.div`
  background-color: ${(props) => props.theme.colors.light};
  border-right: 1px solid ${(props) => props.theme.colors.gray500};
`;

const ChatList = lazy(() => import('src/features/chat/components/ChatList'));

const Chat = () => {
  return (
    <Sidebar className="fixed top-0 left-0 z-40 pt-24 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0">
      <Row centerY marginY="15px">
        <div className="flex justify-between w-full">
          <span className="px-3 font-bold text-lg">{'Chats'}</span>
          <div className="px-3 font-medium text-sm">
            <span className="mr-2">{'+'}</span>
            <span>{'New'}</span>
          </div>
        </div>
      </Row>
      <Row centerY marginY="15px">
        <div className="px-3">
          <InputSearch placeholder="Search contact / chat" />
        </div>
      </Row>
      <Row centerY>
        <ChatList />
      </Row>
    </Sidebar>
  );
};

export default Chat;
